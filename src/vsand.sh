#!/usr/bin/env bash

vsand () {
  local COMMAND=$(cat /dev/stdin);
  local WHITELIST='';
  local VSAND_DISABLE_DEFAULT_WL="${VSAND_DISABLE_DEFAULT_WL:-true}";
  if [ "$VSAND_DISABLE_DEFAULT_WL" != "true" ] && [ "$VSAND_DISABLE_DEFAULT_WL" != "false" ]; then
    VSAND_DISABLE_DEFAULT_WL=true;
  fi;
  local DISABLE_DEFAULT_WL="$VSAND_DISABLE_DEFAULT_WL";

  resolve-wl () {
    if echo "$1" | egrep -q "(^,|,$|[^a-zA-Z0-9_,])"; then
      echo "Whitelist format is invalid. Only alphanumeric variable names comma separated are allowed.";
      exit 1;
    fi
    for VARIABLE in ${1//,/ }
    do
      if [[ -z $WHITELIST ]]; then
        WHITELIST="$VARIABLE=\"\$$VARIABLE\"";
      else
        WHITELIST="$WHITELIST $VARIABLE=\"\$$VARIABLE\"";
      fi
    done
  }

  while getopts 'dW:' FLAG; do
    case "${FLAG}" in
      d) DISABLE_DEFAULT_WL='true' ;;
      W) resolve-wl $OPTARG ;;
      ?) exit 1 ;;
    esac
  done

  if [ $DISABLE_DEFAULT_WL == 'false' ]; then
    if [[ ! -z $VSAND_WHITELIST ]]; then
      resolve-wl $VSAND_WHITELIST;
    fi
  fi
  if [[ ! -z $WHITELIST ]]; then
    eval "env -i HOME=\"\$HOME\" $WHITELIST bash -l -r -c \"\$COMMAND\"";
  else
    env -i HOME="$HOME" bash -l -r -c "$COMMAND";
  fi
}
