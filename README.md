# vsand
`vsand` is a `v`ariables `sand`box. This command is meant to be used inside a gitlab runner to execute commands in a sub shell with restricted bash and with a fresh environment, so you can hide variables with credentials or any sensible data.  

The main problem with gitlab-ci variables is when the variables are holding credentials. You can mask them so there will never be shown in any job log, but if someone stoles a developer account and changes the `.gitlab-ci.yml` file it can easily send the credentials to a malicious service. You can protect that file with CODEOWNERS forcing to be revewed if someone wants to change it (only available in public SaaS projects or premium+ tier) and by disabling some gitlab features (only possible if you have a self hosted gitlab) to prevent anyone to change protected branches rules and the default `.gitlab-ci.yml` location (if you haven't a self hosted gitlab, you must limit the maintainers and/or ensure they will not modify those features).  
Now, you can limit the users that can change `.gitlab-ci.yml`, but if you have a job that executes a command who could be written by a developer you just are keeping the same problem, the malicious "developer" could access to your credentials variables. Lets see an example:  

`.gitlab-ci.yml`
```yml
build:
  script:
    - ./build.sh
```

`build.sh`
```sh
#!/usr/bin/env bash
send $CI_REGISTRY_USER and $CI_REGISTRY_PASSWORD to http://malicious.service
```

To prevent this, you can use `vsand` inside the `gitlab-ci` jobs, so the credentials will be available only in the job script and not in the developer script:  

`.gitlab-ci.yml`
```yml
build:
  script:
    - vsand <<< "
    - ./build.sh
    - "
```

If only a few confidence maintainers can change `.gitlab-ci.yml` it doesn't matters if a developer changes `build.sh`, they will not be able to access the credentials variables because `vsand` is hiding them.

## Install
To install `vsand`, you must clone it from git and `source` the install script.

```sh
git clone https://gitlab.com/while-true/ci-cd/vsand.git && source './vsand/install.sh';
```

This will install the src inside the path `VSAND_DEST` (default: /usr/local/src/vsand) and then will add it to the profile `VSAND_PROFILE` (default: /etc/profile.d/vsand-profile.sh) so you need permission to access those directories.

If you want to add `vsand` just for the current bash session, you must use the `-s` flag to just load the source:

```sh
git clone https://gitlab.com/while-true/ci-cd/vsand.git && source './vsand/install.sh' -s;
```

You don't need access to any folder when `-s` flag is present, but the command will dissapear when the bash session ends and will not be available in any child bash session.

## Usage
`vsand [OPTION] <here-document> or <here-string>`.  

Options:  
* -d Disable default variables whitelist.  
* -W=WHITELIST A comma separated whitelist of variables names to don't hide.  

Variables:  
* `VSAND_WHITELIST`: A comma separated whitelist of variables names to don't hide. This whitelist will be used globally by `vsand` when `VSAND_DISABLE_DEFAULT_WL` is set to "false".
* `VSAND_DISABLE_DEFAULT_WL` (default: true): Flag to globally enable/disable the use of `VSAND_WHITELIST`.

## Default whitelist
Here, the default variables whitelisted by `VSAND_WHITELIST`:
```
CHAT_CHANNEL  
CHAT_INPUT  
CI  
CI_API_V4_URL  
CI_BUILDS_DIR  
CI_COMMIT_BEFORE_SHA  
CI_COMMIT_DESCRIPTION  
CI_COMMIT_MESSAGE  
CI_COMMIT_REF_NAME  
CI_COMMIT_REF_PROTECTED  
CI_COMMIT_REF_SLUG  
CI_COMMIT_SHA  
CI_COMMIT_SHORT_SHA  
CI_COMMIT_BRANCH  
CI_COMMIT_TAG  
CI_COMMIT_TITLE  
CI_COMMIT_TIMESTAMP  
CI_CONCURRENT_ID  
CI_CONCURRENT_PROJECT_ID  
CI_CONFIG_PATH  
CI_DEBUG_TRACE  
CI_DEFAULT_BRANCH  
CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX  
CI_DEPENDENCY_PROXY_SERVER  
CI_DEPLOY_FREEZE  
CI_DISPOSABLE_ENVIRONMENT  
CI_ENVIRONMENT_NAME  
CI_ENVIRONMENT_SLUG  
CI_ENVIRONMENT_URL  
CI_EXTERNAL_PULL_REQUEST_IID  
CI_EXTERNAL_PULL_REQUEST_SOURCE_REPOSITORY  
CI_EXTERNAL_PULL_REQUEST_TARGET_REPOSITORY  
CI_EXTERNAL_PULL_REQUEST_SOURCE_BRANCH_NAME  
CI_EXTERNAL_PULL_REQUEST_SOURCE_BRANCH_SHA  
CI_EXTERNAL_PULL_REQUEST_TARGET_BRANCH_NAME  
CI_EXTERNAL_PULL_REQUEST_TARGET_BRANCH_SHA  
CI_HAS_OPEN_REQUIREMENTS  
CI_OPEN_MERGE_REQUESTS  
CI_JOB_ID  
CI_JOB_IMAGE  
CI_JOB_MANUAL  
CI_JOB_NAME  
CI_JOB_STAGE  
CI_JOB_STATUS  
CI_JOB_URL  
CI_KUBERNETES_ACTIVE  
CI_MERGE_REQUEST_ASSIGNEES  
CI_MERGE_REQUEST_ID  
CI_MERGE_REQUEST_IID  
CI_MERGE_REQUEST_LABELS  
CI_MERGE_REQUEST_MILESTONE  
CI_MERGE_REQUEST_PROJECT_ID  
CI_MERGE_REQUEST_PROJECT_PATH  
CI_MERGE_REQUEST_PROJECT_URL  
CI_MERGE_REQUEST_REF_PATH  
CI_MERGE_REQUEST_SOURCE_BRANCH_NAME  
CI_MERGE_REQUEST_SOURCE_BRANCH_SHA  
CI_MERGE_REQUEST_SOURCE_PROJECT_ID  
CI_MERGE_REQUEST_SOURCE_PROJECT_PATH  
CI_MERGE_REQUEST_SOURCE_PROJECT_URL  
CI_MERGE_REQUEST_TARGET_BRANCH_NAME  
CI_MERGE_REQUEST_TARGET_BRANCH_SHA  
CI_MERGE_REQUEST_TITLE  
CI_MERGE_REQUEST_EVENT_TYPE  
CI_MERGE_REQUEST_DIFF_ID  
CI_MERGE_REQUEST_DIFF_BASE_SHA  
CI_NODE_INDEX  
CI_NODE_TOTAL  
CI_PAGES_DOMAIN  
CI_PAGES_URL  
CI_PIPELINE_ID  
CI_PIPELINE_IID  
CI_PIPELINE_SOURCE  
CI_PIPELINE_TRIGGERED  
CI_PIPELINE_URL  
CI_PROJECT_CONFIG_PATH  
CI_PROJECT_DIR  
CI_PROJECT_ID  
CI_PROJECT_NAME  
CI_PROJECT_NAMESPACE  
CI_PROJECT_ROOT_NAMESPACE  
CI_PROJECT_PATH  
CI_PROJECT_PATH_SLUG  
CI_PROJECT_REPOSITORY_LANGUAGES  
CI_PROJECT_TITLE  
CI_PROJECT_URL  
CI_PROJECT_VISIBILITY  
CI_REGISTRY  
CI_REGISTRY_IMAGE  
CI_RUNNER_DESCRIPTION  
CI_RUNNER_EXECUTABLE_ARCH  
CI_RUNNER_ID  
CI_RUNNER_REVISION  
CI_RUNNER_TAGS  
CI_RUNNER_VERSION  
CI_SERVER  
CI_SERVER_URL  
CI_SERVER_HOST  
CI_SERVER_PORT  
CI_SERVER_PROTOCOL  
CI_SERVER_NAME  
CI_SERVER_REVISION  
CI_SERVER_VERSION  
CI_SERVER_VERSION_MAJOR  
CI_SERVER_VERSION_MINOR  
CI_SERVER_VERSION_PATCH  
CI_SHARED_ENVIRONMENT  
GITLAB_CI  
GITLAB_FEATURES  
TRIGGER_PAYLOAD  
```
Notice that only a few gitlab [predefined variables](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html) aren't whitelisted here, there are:  
```
GITLAB_USER_NAME  
GITLAB_USER_LOGIN  
GITLAB_USER_ID  
GITLAB_USER_EMAIL  
CI_RUNNER_SHORT_TOKEN  
CI_REPOSITORY_URL  
CI_REGISTRY_USER  
CI_REGISTRY_PASSWORD  
CI_JOB_JWT  
CI_JOB_TOKEN  
CI_DEPLOY_USER  
CI_DEPLOY_PASSWORD  
CI_DEPENDENCY_PROXY_USER  
CI_DEPENDENCY_PROXY_PASSWORD  
```
