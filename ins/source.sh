#!/usr/bin/env bash
# This script must be evaluated as ssh-agent.
# e.g.: eval "$(./vsand/ins/source.sh)"
cd "$(dirname "${BASH_SOURCE[0]}")";
cd ..;
VSAND_DEST="${VSAND_DEST-/usr/local/src/vsand}";
VSAND_PROFILE="${VSAND_PROFILE-/etc/profile.d/vsand-profile.sh}";
ONLY_SOURCE="false";

while getopts 'fs' FLAG; do
  case "${FLAG}" in
    f) rm -rf "$DEST" >/dev/null 2>&1 ;;
    s) ONLY_SOURCE="true" ;;
    ?) exit 1 ;;
  esac
done

if [ "$CI" == "true" ]; then
  sed -i -e 's/mesg n .*true/tty -s \&\& mesg n/g' "$HOME/.profile";
fi

echo "source '$(pwd)/src/defaults.sh'";
echo "source '$(pwd)/src/vsand.sh'";

if [ "$ONLY_SOURCE" == "false" ]; then
  cp -r "$(pwd)/src" "$VSAND_DEST";
  echo "source '$VSAND_DEST/defaults.sh';" > "$VSAND_PROFILE";
  echo "source '$VSAND_DEST/vsand.sh';" >> "$VSAND_PROFILE";
fi

