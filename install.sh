#!/usr/bin/env bash

cd "$(dirname "${BASH_SOURCE[0]}")";
eval "$("$(pwd)/ins/source.sh" "$@")" >/dev/null 2>&1;
"$(pwd)/ins/clean.sh";
cd - >/dev/null 2>&1;
